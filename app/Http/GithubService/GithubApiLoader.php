<?php
/**
 * Created by PhpStorm.
 * User: konrad
 * Date: 15.03.2019
 * Time: 10:39
 */

namespace App\Http\GithubService;

use App\GithubUser;
use GuzzleHttp\Client;

class GithubApiLoader
{

    protected $githubUsers = ['KonradNowacki', 'andrew', 'egoist', 'ornicar', 'nelsonic'];

    /**
     * KonradComments: This function is responsible for getting data from Github API
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGithubUserData($user)
    {
        $client = new Client([
            'base_uri' => 'https://api.github.com/users/'
        ]);

        $response = $client->request('GET', $user)
            ->getBody()
            ->getContents();

        return json_decode($response, true);
    }

    public function storeGithubUserData()
    {
        foreach ($this->githubUsers as $user) {

            $data = $this->getGithubUserData($user);

            $record = new GithubUser();
            $record->login = $data['login'];
            $record->avatar_url = $data['avatar_url'];
            $record->bio = $data['bio'];
            $record->save();
        }
    }
}