<?php

namespace App\Http\Controllers;

use App\GithubUser;
use App\Http\GithubService\GithubApiLoader;

class PagesController extends Controller
{

    protected $api;
    protected $user;

    public function __construct()
    {
        $this->api = new GithubApiLoader();
        $this->user = new GithubUser();
    }

    public function index()
    {
        $response = GithubUser::all()->unique('login');

        return view('pages.index', [
            'response' => $response
        ]);
    }

    public function show($id)
    {
        $response = GithubUser::query()->find($id);

        return view('pages.show', [
            'response' => $response
        ]);
    }

    public function add()
    {
        $this->api->storeGithubUserData();

        return redirect('/');
    }

}
